const today = new Date();
const monthFormat = today.getMonth() > 8 ? (today.getMonth() + 1).toString() : ('0' + (today.getMonth() + 1).toString());
const dateFormat = today.getDate() > 9 ? today.getDate().toString() : ('0' + today.getDate());
export const todaysDate = monthFormat + dateFormat + today.getFullYear();
