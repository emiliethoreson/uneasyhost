import './App.css';
import { useState, useEffect } from 'react';
import { doc, getFirestore, getDoc, setDoc, updateDoc } from 'firebase/firestore';
import { initializeApp } from 'firebase/app';
import StartGameScreen from './Components/StartGameScreen';
import RoundContent from './Components/RoundContent';
import { todaysDate } from './utils/todaysDate';

function App() {
  const [currentRound, setCurrentRound] = useState(0);
  const gameCode = localStorage.getItem('gameCode');
  const roundOrder = [0, 1, 2, 3, 4, 5, 6, 'final', 7];

  const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_API,
    authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGE_SENDER_ID,
    appId: process.env.REACT_APP_FIREBASE_ID,
    measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
  }
  
  const app = initializeApp(firebaseConfig);
  const db = getFirestore(app);
  
  const nextRound = () => {
    const index = roundOrder.indexOf(currentRound);
    setDoc(doc(db, `${todaysDate}-${gameCode}`, 'meta'), {
      currentRound: roundOrder[index + 1]
    }, { merge: true })
  setCurrentRound(roundOrder[index + 1]);
  }

  const adminReset = () => {
    let codePrompt = window.prompt("enter password for reset:", "");
    if (codePrompt === 'roomff') {
      let room = window.prompt("enter level:", "");
      let formattedRoom = parseFloat(room);
      setDoc(doc(db, `${todaysDate}-${gameCode}`, 'meta'), {
        currentRound: formattedRoom
      }, { merge: true })
      setCurrentRound(formattedRoom)
      }
    if (codePrompt === 'hardreset') {
      localStorage.setItem('gameCode', undefined);
      setCurrentRound(0);
    }
  }

  const loadDisplay = () => {
    let display;
    if (currentRound === 0 || currentRound === undefined) {
        display = (<StartGameScreen startNewGame={() => {setCurrentRound(1)}} adminReset={adminReset} checkForExistingGame={checkForExistingGame} app={app} db={db} />);
    } else {
        display = (<RoundContent currentRound={currentRound} db={db} nextRound={nextRound} key={currentRound} roundOrder={roundOrder} />);
    }
    return display;
  }

  async function checkForExistingGame() {
    if (gameCode && gameCode.length === 4) {
      const docSnap = await getDoc(doc(db, `${todaysDate}-${gameCode}`, 'meta'));
      if (docSnap.exists()) {
        const data = docSnap.data();
        setCurrentRound(data.currentRound);
      } else {
        setCurrentRound(0);
      }
    } else {
      setCurrentRound(0);
    }
  }

  useEffect(() => {
    checkForExistingGame();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        {typeof gameCode === 'string' && gameCode !== 'undefined' ? <span id='room-code-display'>Game code: {gameCode}</span> : <></>}
      </header>
      <div className='content-skeleton'>
        {loadDisplay()}
      </div>
      <div className='footer'>
        <span className='footer-label'>Uneasy Events host grading portal v2.0.4</span>
        <button onClick={adminReset}>◁ ▷</button>
      </div>
    </div>
  );
}

export default App;
