import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { doc, collection, getDocs, setDoc, serverTimestamp } from 'firebase/firestore';
import { Blocks } from 'react-loader-spinner';
import './StartGameScreen.css';
import { todaysDate } from '../utils/todaysDate';

function StartGameScreen(props) {
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    const [availableGames, setAvailableGames] = useState([]);
    const [selectedGame, setSelectedGame] = useState('');
    const [showGameInProgressPrompt, setShowGameInProgressPrompt] = useState(false);

    useEffect(() => {
        async function loadGameSelection() {
            let gameNames = [];
            const snapshot = await getDocs(collection(props.db, 'activeGames'));
            snapshot.forEach((doc) => {
                gameNames.push({ label: doc.id, value: doc.id});
            })
            setAvailableGames(gameNames);
        }
        loadGameSelection();
    }, []);


    async function signInByHost(e) {
        e.preventDefault();
        setLoading(true);
        if (selectedGame === '') {
            alert('Please select a game from the dropdown menu.');
            return;
        }
        const email = document.getElementById('hostid').value;
        const pass = document.getElementById('hostpass').value;
        const gameCode = createGameCode();
        const auth = getAuth(props.app);
        signInWithEmailAndPassword(auth, email, pass)
        .then(() => {
            copyCollection(gameCode)
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.error(errorCode, errorMessage);
            setError(true)
        });
    }

    async function copyCollection(gameCode) {
        const querySnapshot = await getDocs(collection(props.db, 'activeGames', selectedGame, 'rounds'));
        querySnapshot.forEach((doc) => {
            createNewGameDoc(gameCode, doc);
            if (doc.id === 'teams') {
                createMetaDoc(gameCode)
            }
        }) 
    }

    async function createNewGameDoc(gameCode, templateDoc) {
        const data = templateDoc.data();
        await setDoc(doc(props.db, `${todaysDate}-${gameCode}`, templateDoc.id), data)
    }

    const createGameCode = () => {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let counter = 0;
        while (counter < 4) {
            result += characters.charAt(Math.floor(Math.random() * 26));
            counter += 1;
        }
        localStorage.setItem('gameCode', result);
        return result;
    }

    async function createMetaDoc(gameCode) {
        const loginHost = document.getElementById('hostid').value;
        try {
            await setDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'meta'), {
                gameCode: gameCode,
                host: loginHost,
                currentRound: 1,
                startedAt: serverTimestamp()
            }, { merge: true });
            props.startNewGame()
        } catch (error) {
            console.error("Error creating meta doc: ", error);
        }
    }

    const joinExistingGame = (e) => {
        e.preventDefault();
        setLoading(true);
        const email = document.getElementById('hostid').value;
        const pass = document.getElementById('hostpass').value;
        const activegameCode = document.getElementById('activeGameCode').value;
        localStorage.setItem('gameCode', activegameCode);
        const auth = getAuth(props.app);
        signInWithEmailAndPassword(auth, email, pass)
        .then(() => {
            props.checkForExistingGame();
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorCode, errorMessage);
            alert('Invalid email or password')
            setError(true)
            setLoading(false);
        });
    }

    const handleGameSelection = e => {
        setSelectedGame(e.value);
    }

    return (
        <div>
        <div className='sign-up-skeleton'>
            <img className='full-logo' id='start-screen-logo' src={require('../img/full_logo.png')} />
            <div id='sign-in-form'>
            <form onSubmit={signInByHost} className='sign-in-form'>
                <div className='sign-in-field'>
                <label className='sign-in-label'>Email:</label>
                <input type="email" required name='email' id='hostid' autoComplete='on'></input>
                </div>
                <div className='sign-in-field'>
                <label className='sign-in-label'>Password:</label>
                <input type="password" required name='pass' id='hostpass'></input>
                </div>
                <div className='sign-in-field'>
                <label className='sign-in-label'>Game:</label>
                    <Select
                        options={ availableGames }
                        onChange={handleGameSelection}
                        value={availableGames.filter(function(option) {
                            return option.value === selectedGame;
                        })}
                    />
                </div>
                <div className='sign-in-field'><button type="submit" className='submit-button'>Start Game</button></div>
                {error && <div className='error'>Your email or password does not match our records. Double-check, then call: 971-336-0514.</div>}
            </form>
            <button className='join-game-in-progress-link' onClick={() => {setShowGameInProgressPrompt(true)}}>or: go to game in progress</button>
            </div>
            {showGameInProgressPrompt && <div className='join-existing-game-form'>
            <form onSubmit={joinExistingGame}>
            <label className='sign-in-label'>Email:</label>
                <input type="email" required name='email' id='hostid' autoComplete='on'></input>
                <label className='sign-in-label'>Password:</label>
                <input type="password" required name='pass' id='hostpass'></input>
                <label className='sign-in-label'>Room code:</label>
                <input type="text" id='activeGameCode'></input>
                <button type="submit" className='submit-button'>Rejoin Game</button>
            </form>
            </div>}
        </div>
        {loading && <Blocks
            height="300"
            width="300"
            color="#8a0e67"
            ariaLabel="blocks-loading"
            wrapperStyle={{}}
            wrapperClass="blocks-wrapper"
            visible={true}
        />}
        </div>
    );
}

export default StartGameScreen;