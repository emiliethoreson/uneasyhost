import React, { useState, useEffect } from 'react';
import { getDoc, doc, onSnapshot } from "firebase/firestore";
import { todaysDate } from '../utils/todaysDate';
import QuestionDisplay from './QuestionDisplay.js';
import './RoundQuestions.css';

function Wager(props) {
    const [loaded, setLoaded] = useState([]);
    const [hideButton, setHideButton] = useState(false);
    const [bets, setBets] = useState([]);
    const gameCode = localStorage.getItem('gameCode');

    useEffect(() => {
        async function loadQuestions() {
            const docSnap = await getDoc(doc(props.db,  `${todaysDate}-${gameCode}`, 'round-final'));
            if (docSnap.exists()) {
                const data = docSnap.data();
                setLoaded(<div key={1}>
                    <QuestionDisplay data={data[1]} qNum='1' currentRound='final' gameCode={gameCode} db={props.db} />
                </div>)
            } else {
                alert(`We could not find round ${props.stage} under game code ${gameCode}. Please call Emilie: 971-336-0514`);
            }
        }
        onSnapshot(doc(props.db, `${todaysDate}-${gameCode}`, 'bets'), (doc) => {
            if (doc.data()) {
                const data = doc.data();
                processWagers(data);
            }
        });
        loadQuestions();
    }, [props.currentRound, hideButton]);

    const processWagers = (rawData) => {
        let incomingBets = [];
        for (const team in rawData) {
            incomingBets.push(<div key={team}>{team} :: {rawData[team]}</div>);
        }
        setBets(incomingBets);
    }

    return (
        <div className='content-column round-questions-skeleton'>
            <h4>Final wagers:</h4>
            <ol type='1'>{bets}</ol>
            <h6>Final question:</h6>
            <div>{loaded}</div>
        </div>
    );
}

export default Wager;
