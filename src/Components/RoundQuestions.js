import React, { useState, useEffect } from 'react';
import { doc, getDoc } from 'firebase/firestore';
import QuestionDisplay from './QuestionDisplay';
import { todaysDate } from '../utils/todaysDate';
import './RoundQuestions.css';

function RoundQuestions(props) {
    const [loaded, setLoaded] = useState([]);
    const [roundTitle, setRoundTitle] = useState('');
    const [roundDesc, setRoundDesc] = useState('');
    const [furtherExplanation, setFurtherExplanation] = useState('');
    const gameCode = localStorage.getItem('gameCode');

    useEffect(() => {
        async function loadQuestions() {
            const docSnap = await getDoc(doc(props.db, `${todaysDate}-${gameCode}`, `round-${props.currentRound}`));
            if (docSnap.exists()) {
                const data = docSnap.data();
                let questions = [];
                const qs = Object.keys(data);
                if (qs.length) {
                    qs.map((currentQ) => {
                        if (currentQ !== '8') {
                        questions.push(<div key={currentQ}>
                            <QuestionDisplay data={data[currentQ]} qNum={currentQ} currentRound={props.currentRound} gameCode={gameCode} db={props.db} />
                        </div>)
                        } else {
                            if (data[currentQ][0]) {
                                setRoundTitle(data[currentQ][0])
                            }
                            if (data[currentQ][1]) {
                                setRoundDesc(data[currentQ][1])
                            }
                            if (data[currentQ][2]) {
                                setFurtherExplanation(data[currentQ][2])
                            }
                        }
                    })
                    setLoaded(questions);
                }
            } else {
                alert(`There is no round ${props.currentRound} under game code ${gameCode}. Please call Emilie: 971-336-0514`);
            }
        }
        loadQuestions();
    }, [props.currentRound]);

    return (
        <div className='content-column round-questions-skeleton'>
            {roundTitle && <h4 className='round-title'>{roundTitle}</h4>}
            {roundDesc && <h6>{roundDesc}</h6>}
            {furtherExplanation && <h6 className='host-hint'>{furtherExplanation}</h6>}
            {loaded}
        </div>
    );
}

export default RoundQuestions;