import React, { useMemo, useState } from 'react';
import { onSnapshot, doc, getDoc, updateDoc } from 'firebase/firestore';
import IncomingToBeGraded from './IncomingToBeGraded';
import Countdown from 'react-countdown';
import { todaysDate } from '../utils/todaysDate';

function IncomingWagers(props) {
    const [displayQuestions, setDisplayQuestions] = useState([]);
    const [collectedResults, setCollectedResults] = useState([]);
    const [timer, setTimer] = useState([]);
    const [startTimer, setStartTimer] = useState(false);
    const gameCode = localStorage.getItem('gameCode');

    useMemo(() => {
        setTimer(<Countdown date={Date.now() + 120000} />);
    }, [startTimer]);

    async function saveRound() {
        const teamsSnap = await getDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams'));
        if (teamsSnap.exists()) {
            let shallowTeams = teamsSnap.data();
                applyWagerRoundPoints(shallowTeams);
        }
    }

    async function listenForAnswers() {
        setStartTimer(true);
        const round = props.currentRound;
        onSnapshot(doc(props.db,  `${todaysDate}-${gameCode}`, 'teams', 'round-final', 'answers'), (doc) => {
            if (doc.data()) {
                const data = doc.data();
                processAnswers(data);
            }
        });
    }

    const processAnswers = (rawData) => {
        let incoming = [];
            const rawDataKeys = Object.keys(rawData[1]);
            incoming.push(
                <IncomingToBeGraded 
                    guesses={rawData[1]}
                    numOfGuesses={rawDataKeys.length}
                    questionNum='final'
                    saveQuestionResults={applyWagerPoints}
                    round='final'
                    db={props.db}
                />
            )
            setDisplayQuestions(incoming);
    }

    const applyWagerPoints = (teams) => {
        let newResults = collectedResults;
        newResults[props.currentRound] = teams;
        setCollectedResults(newResults);
    }
    
    async function applyWagerRoundPoints(shallowTeams) {
        let newTotals = {};
        const betsSnap = await getDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'bets'));
        if (shallowTeams && betsSnap.exists()) {
            const teamNames = Object.keys(shallowTeams);
            console.log(collectedResults['final'])
            teamNames.forEach((team) => {
                console.log(collectedResults['final'].includes(team))
                const betsData = betsSnap.data();
                const teamBet = betsData[team];
                console.log('team ', team, 'bet ', teamBet)
                    if (collectedResults['final'].includes(team)) {
                        console.log(team, ' got it right, adding ', teamBet)
                        newTotals[team] = shallowTeams[team] + teamBet;
                    } else {
                        if (typeof teamBet === 'number') {
                            console.log(console.log(team, ' got it wrong, subtracting ', teamBet)
                        )
                            newTotals[team] = shallowTeams[team] - teamBet;
                        } else {
                            console.log(team, ' didnt bet, subtracting 5')
                            newTotals[team] = shallowTeams[team] - 5
                        }
                    }
                    saveToFirebase(newTotals);

            });
        }
    }
    
    async function saveToFirebase(totals) {
        try {
            await updateDoc(doc(props.db,  `${todaysDate}-${gameCode}`, 'teams'), totals)
            .then(() => {
                props.nextRound();
            });
        } catch (e) {
            console.log('error:', e)
        }
    }

    return (
        <div className='content-column incoming-answers-skeleton'>
            <h4>Incoming Final Answers</h4>
            <div className='timer-block'>
                {!startTimer && <button onClick={listenForAnswers} className="listen-action">Start timer</button>}
                {startTimer && timer}
            </div>
            <div>{displayQuestions}</div>
            <div className='next-round'>
                <button className='save-round' onClick={saveRound}>Calculate totals</button>
            </div>
        </div>
    );
}

export default IncomingWagers;
