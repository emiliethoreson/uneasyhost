import React from 'react'
import Countdown from 'react-countdown'

export default function CountdownDisplay() {
    return <Countdown date={Date.now() + 300000} />
}