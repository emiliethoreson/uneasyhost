import React, { useState } from 'react';
import { doc, updateDoc } from 'firebase/firestore';
import './QuestionDisplay.css';
import { todaysDate } from '../utils/todaysDate';

function QuestionDisplay(props) {
    const [hideButton, setHideButton] = useState(false);
    const gameCode = localStorage.getItem('gameCode');

    async function releaseQ(questionArr, i) {
        const questionRef = doc(props.db, `${todaysDate}-${gameCode}`, `round-${props.currentRound}`);
        await updateDoc(questionRef, {
            [i]: [questionArr[0], true, questionArr[2]]
        })
        .then(
            setHideButton(true)
        )
    }

    return (
        <>
            <div className='individual-question'>{props.qNum}. {props.data[0]}</div>
            {props.data[2] && <div className='host-hint'>{props.data[2]}</div>}
            {!props.data[1] && !hideButton && <button onClick={() => releaseQ(props.data, props.qNum)} className='release-question-button'>Release</button>}
        </>
    );
}

export default QuestionDisplay;

