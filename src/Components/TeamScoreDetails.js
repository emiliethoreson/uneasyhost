import React, { useEffect, useState } from 'react';
import { doc, getDoc, setDoc, query } from 'firebase/firestore';
import { todaysDate } from '../utils/todaysDate';
import './TeamScoreDetails.css';

function TeamScoreDetails(props) {
    const [teamDetails, setTeamDetails] = useState([]);
    const [teamTotal, setTeamTotal] = useState(0);
    const gameCode = localStorage.getItem('gameCode');

    const closeModal = () => {
        props.closeModal();
    }

    async function getDetails() {
        let teamDisplay = [];
        const docSnap = await getDoc(query(doc(props.db, `${todaysDate}-${gameCode}`, 'teams', 'teamNames', `${props.teamName}`)));
        if (docSnap.exists()) {
            const teamData = docSnap.data();
            for (const round in teamData) {

                    teamDisplay.push(<div className='team-round-details' key={props.teamName + round + '-display'}>
                    <h5>Round {round}</h5>
                    <ol>
                        <li key={props.teamName + round + '1'}>{teamData[round][1][0]} :: {teamData[round][1][1]}</li>
                        <li key={props.teamName + round + '2'}>{teamData[round][2][0]} :: {teamData[round][2][1]}</li>
                        <li key={props.teamName + round + '3'}>{teamData[round][3][0]} :: {teamData[round][3][1]}</li>
                        <li key={props.teamName + round + '4'}>{teamData[round][4][0]} :: {teamData[round][4][1]}</li>
                        <li key={props.teamName + round + '5'}>{teamData[round][5][0]} :: {teamData[round][5][1]}</li>
                        <li key={props.teamName + round + '6'}>{teamData[round][6][0]} :: {teamData[round][6][1]}</li>
                        <li key={props.teamName + round + '7'}>{teamData[round][7][0]} :: {teamData[round][7][1]}</li>
                    </ol>
                </div>)
            }
            setTeamDetails(teamDisplay);
        } else {
            console.log('could not load teams at this time');
        }
    }

    async function getTotal() {
        const teamsSnap = await getDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams'));
        if (teamsSnap.exists()) {
            let shallowTeams = teamsSnap.data();
            const shallowTotal = parseInt(shallowTeams[props.teamName]);
            setTeamTotal(shallowTotal);
        }
    }

    const saveNewTotal = (e) => {
        e.preventDefault();
        setDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams'), {
            [props.teamName]: parseInt(e.target[0].value)
        }, { merge: true })
        closeModal();
    }

    useEffect(() => {
        getDetails();
        getTotal();
    }, []);

    return (
        <div className='team-score-details-skeleton'>
            <div className='team-score-details-content'>
            <button className='close' onClick={closeModal}>x</button>
            <div className='rounds-list'>
                <h4>{props.teamName}</h4>
                {teamDetails}
            </div>
            <form onSubmit={saveNewTotal}>
                <label>Total:</label>
                <input type='number' name='teamTotal' placeholder={teamTotal}></input>
                <button type='submit'>Save</button>
            </form>
            </div>
        </div>
    );
}

export default TeamScoreDetails;