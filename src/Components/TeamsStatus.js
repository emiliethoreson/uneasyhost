import React, { useState, useEffect } from 'react';
import { getDoc, doc } from 'firebase/firestore';
import TeamScoreDetails from './TeamScoreDetails';
import { todaysDate } from '../utils/todaysDate';
import './TeamsStatus.css';

function TeamsStatus(props) {
    const gameCode = localStorage.getItem('gameCode');
    const [teams, setTeams] = useState([]);
    const [showTeam, setShowTeam] = useState('');

    const loadTeamDetails = e => {
        e.preventDefault();
        if (showTeam === e.target.value) { 
            setShowTeam(''); 
            return; 
        } else {
            setShowTeam(e.target.value);
        }
    }

    async function loadTeamTotals() {
        let displayTeams = [];
        const docSnap = await getDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams'));
        if (docSnap.exists()) {
            const unformattedTeams = docSnap.data();
            if (unformattedTeams) {
                let slightlyFormatted = Object.entries(unformattedTeams).sort((a, b) => b[1] - a[1]);
                slightlyFormatted.forEach((team) => {
                    displayTeams.push(<li key={team[0] + '-total'} className='team-score-display'>
                            <div><span className='team-name-span'>{team[0]}</span>{team[1]} <button name='team-name' value={team[0]} onClick={loadTeamDetails}>≡</button></div>
                    </li>)
                })
                setTeams(displayTeams);
            }
        } else {
            console.log(`could not load ${gameCode} teams`);
        }
    }

    useEffect(() => {
        loadTeamTotals();
    }, [props.currentRound])

    return (
        <div className='content-column teams-column-skeleton'>
            <h4>Teams</h4>
            <ol type='1' className='teams-ordered-list'>{teams}</ol>
            <button onClick={loadTeamTotals} className='show-total-scores-button'>↻</button>
            {showTeam &&
                <TeamScoreDetails teamName={showTeam} closeModal={() => setShowTeam('')} db={props.db} />
            }
        </div>
    );
}


export default TeamsStatus;