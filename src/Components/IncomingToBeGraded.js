import React, { useState, useEffect } from 'react';
import { doc, setDoc } from "firebase/firestore";
import GuessToBeGraded from './GuessToBeGraded';
import './IncomingToBeGraded.css';
import { todaysDate } from '../utils/todaysDate';

function IncomingToBeGraded(props) {
    const [display, setDisplay] = useState([]);
    const [correctAnswers, setCorrectAnswers] = useState([]);
    const [incorrectAnswers, setIncorrectAnswers] = useState([]);
    const [gradingDisabled, setGradingDisabled] = useState(false);
    const gameCode = localStorage.getItem('gameCode');

    async function displayResults() {
        let gets = [];
        for (let i = 0; i < correctAnswers.length; i++) {
            const teamsWithThisAnswer = Object.keys(props.guesses).filter(item => props.guesses[item] === correctAnswers[i]);
            gets.push(teamsWithThisAnswer);
            teamsWithThisAnswer.forEach((team) => {
                if (props.round !== 'final') {
                    setPlusOne(team, i)
                }
            })
        }
        setGradingDisabled(true);
        props.saveQuestionResults(gets.flat(), props.questionNum);
    }

    async function setPlusOne(team, i) {
        setDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams', 'teamNames', team), {
            [props.round]: {[props.questionNum]: [correctAnswers[i], 1]}
        }, { merge: true })
    }

    useEffect(() => {
        let loaded = [];
        if (gradingDisabled) {
            loaded.push(<div><button onClick={() => {setGradingDisabled(false)}}>Unlock {props.questionNum}</button></div>)
        } else {
        const answers = Object.values(props.guesses);
        const counts = {};
        answers.forEach((guess) => {
            counts[guess] = (counts[guess] || 0) + 1;
        });
        const noDuplicateAnswers = Object.keys(counts);
        noDuplicateAnswers.forEach((answer) => {
            const teamsThatGuessed = Object.keys(props.guesses).filter(key => props.guesses[key] === answer).join(', ')
            loaded.push(<GuessToBeGraded
                answer={answer}
                teamsThatGuessed={teamsThatGuessed}
                questionNum={props.questionNum}
                correctAnswers={correctAnswers}
                setCorrectAnswers={setCorrectAnswers}
                incorrectAnswers={incorrectAnswers}
                setIncorrectAnswers={setIncorrectAnswers}
            />)
        })
        }
        setDisplay(loaded);
    }, [props.guesses, gradingDisabled]);

    return (<div key={props.numOfGuesses + '-' + props.questionNum} className='question-collection' id={props.numOfGuesses + '-' + props.questionNum}>
        <div className='question-num' key={props.questionNum}>{props.questionNum}:</div>
        <fieldset className='fieldset'>{display}</fieldset>
        {!gradingDisabled && <button className='show-q-results' onClick={displayResults}>Lock question {props.questionNum}</button>}
    </div>)
}

export default IncomingToBeGraded;