import React from 'react';
import './TeamsStatus.css';
import './IncomingToBeGraded.css';

function GuessToBeGraded(props) {
    const answer = props.answer;

    const addCorrectAnswer = (e) => {
        e.preventDefault();
        let corrects = props.correctAnswers;
        if (corrects.includes(e.target.value)) {
            const index = corrects.indexOf(e.target.value);
            corrects.splice(index, 1);
            e.target.classList.remove('selected');
        } else {
            corrects.push(e.target.value);
            e.target.classList.add('selected');
        }
        props.setCorrectAnswers(corrects);
    }

    const addIncorrectAnswer = (e) => {
        e.preventDefault();
        let incorrects = props.incorrectAnswers;
        if (incorrects.includes(e.target.value)) {
            const index = incorrects.indexOf(e.target.value);
            incorrects.splice(index, 1);
            e.target.classList.remove('no-selected');
        } else {
            incorrects.push(e.target.value);
            props.setIncorrectAnswers(incorrects);
            e.target.classList.add('no-selected');
        }
    }

    return (
        <div className='guess-col' key={props.questionNum + answer}>
            <div className='guess'>{answer}</div>
            <span className='teams-that-guessed' key={props.questionNum + props.teamsThatGuessed}>{props.teamsThatGuessed}</span>
            <div className='buttons'>
                <button key={props.questionNum + 'yes'} className={props.correctAnswers.includes(answer) ? 'selected button' : 'button'} name={props.questionNum} value={answer} onClick={addCorrectAnswer}>✓</button>
                <button key={props.questionNum + 'no'} className={props.incorrectAnswers.includes(answer) ? 'no-selected button' : 'button'} name={props.questionNum} value={answer} onClick={addIncorrectAnswer}>✗</button>
            </div>
        </div>
    );
}


export default GuessToBeGraded;