import React, { useMemo, useState } from 'react';
import { doc, getDoc, onSnapshot, updateDoc, serverTimestamp } from "firebase/firestore";
import IncomingToBeGraded from './IncomingToBeGraded';
import Countdown from 'react-countdown';
import { todaysDate } from '../utils/todaysDate';
import './IncomingAnswers.css';

function IncomingAnswers(props) {
    const [displayQuestions, setDisplayQuestions] = useState([]);
    const [collectedResults, setCollectedResults] = useState([]);
    const [timer, setTimer] = useState([]);
    const [startTimer, setStartTimer] = useState(false);
    const [numberOfSubmissions, setNumberOfSubmissions] = useState(0);
    const gameCode = localStorage.getItem('gameCode');

    useMemo(() => {
        setTimer(<Countdown date={Date.now() + 300000} />);
    }, [startTimer]);

    const doubleCheck = () => {
        if (collectedResults.length !== 7 && props.currentRound < 7) {
            if (window.confirm('It looks like not all questions have been sealed. Are you sure you want to continue?')) {
                saveRound()
            } else {
                return;
            }
        } else {
            saveRound()
        }
    }

    async function saveRound() {
        const teamsSnap = await getDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams'));
        if (teamsSnap.exists()) {
            let shallowTeams = teamsSnap.data();
                applyRegRoundPoints(shallowTeams)
        } else {
            console.error('could not get teams ref')
        }
    }

    async function applyRegRoundPoints(shallowTeams) {
        let newTotals = {};
        let teamNames = Object.keys(shallowTeams)
        teamNames.forEach((team) => {
            let roundTotal = collectedResults.filter((hasCorrectAnswer) => hasCorrectAnswer.includes(team)).length;
            newTotals[team] = shallowTeams[team] + roundTotal;
        });
        saveToFirebase(newTotals);
    }

    async function saveToFirebase(totals) {
        try {
            await updateDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams'), totals)
            .then(() => {
                props.nextRound();
            });
        } catch (e) {
            console.log('error:', e)
        }
    }

    async function listenForAnswers() {
        setStartTimer(true);
        const round = props.currentRound;
        onSnapshot(doc(props.db, `${todaysDate}-${gameCode}`, 'teams', `round-${round}`, 'answers'), (doc) => {
            if (doc.data()) {
                const data = doc.data();
                processAnswers(data);
            }
        });
    }

    const applyPoints = (teams, qNum) => {
        let newResults = collectedResults;
        newResults[qNum - 1] = teams;
        setCollectedResults(newResults);
    }

    const processAnswers = (rawData) => {
        let incoming = [];
        for (let i = 1; i <= Object.keys(rawData).length; i++) {
            setNumberOfSubmissions(Object.keys(rawData['1']).length);
            incoming.push(
                <IncomingToBeGraded 
                    guesses={rawData[JSON.stringify(i)]}
                    numOfGuesses={Object.keys(rawData['1']).length}
                    questionNum={i}
                    saveQuestionResults={applyPoints}
                    round={props.currentRound}
                    db={props.db}
                />
            )
            setDisplayQuestions(incoming);
        }
    }

    async function saveEndTimestamp() {
        await updateDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'meta'), {
            endedAt: serverTimestamp()
        })
    }

    const endGame = () => {
        if (window.confirm("Are you sure all winners have been announced and you're ready to end the game?")) {
            saveEndTimestamp();
            localStorage.clear();
            window.location.reload();
        } else {
            return;
        }
    }

    return (
        <div className='content-column incoming-answers-skeleton'>
            <h4>Incoming Answers</h4>
            <div className='timer-block'>
                {!startTimer && <button onClick={listenForAnswers} className="listen-action">Start timer</button>}
                {startTimer && timer}
                <div className='number-of-submissions'>Teams submitted: {numberOfSubmissions}</div>
            </div>
            <div>{displayQuestions}</div>
            <div className='next-round'>
                {props.currentRound === 7 ?
                <button className='save-round' onClick={endGame}>End game</button> :
                <button className='save-round' onClick={() => {doubleCheck()}}>Next round</button>
                }
            </div>
        </div>
    );
}

export default IncomingAnswers;