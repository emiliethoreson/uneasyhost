import React from 'react';
import RoundQuestions from './RoundQuestions';
import Wager from './Wager';
import IncomingWagerAnswers from './IncomingWagerAnswers';
import IncomingAnswers from './IncomingAnswers';
import TeamsStatus from './TeamsStatus';
import './RoundContent.css';

function RoundContent(props) {

    return (
        <div className='round-content-skeleton'>
            {props.currentRound === 'final' ?
            <><Wager db={props.db} nextRound={props.nextRound} stage={props.currentRound} />
            <IncomingWagerAnswers db={props.db} currentRound={props.currentRound} nextRound={props.nextRound} /></>
            :<>
            <RoundQuestions currentRound={props.currentRound} db={props.db} />
            <IncomingAnswers nextRound={props.nextRound} currentRound={props.currentRound} db={props.db} />
            </>
            }
            <TeamsStatus currentRound={props.currentRound} roundOrder={props.roundOrder} db={props.db} />
        </div>
    );
}

export default RoundContent;